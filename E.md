# E

## Eavesdropping
Eavesdropping is simply listening to a private conversation which may reveal information which can provide access to a facility or network.
## Echo Reply
An echo reply is the response a machine that has received an echo request sends over ICMP.
## Echo Request
An echo request is an ICMP message sent to a machine to determine if it is online and how long traffic takes to get to it.
## Egress Filtering
Filtering outbound traffic.
## Emanations Analysis
Gaining direct knowledge of communicated data by monitoring and resolving a signal that is emitted by a system and that contains the data but is not intended to communicate the data.
## Encapsulation
The inclusion of one data structure within another structure so that the first data structure is hidden for the time being.
## Encryption
Cryptographic transformation of data (called "plaintext") into a form (called "cipher text") that conceals the data's original meaning to prevent it from being known or used.
## Ephemeral Port
Also called a transient port or a temporary port. Usually is on the client side. It is set up when a client application wants to connect to a server and is destroyed when the client application terminates. It has a number chosen at random that is greater than 1023.
## Escrow Passwords
Escrow Passwords are passwords that are written down and stored in a secure location (like a safe) that are used by emergency personnel when privileged personnel are unavailable.
## Ethernet
The most widely-installed LAN technology. Specified in a standard, IEEE 802.3, an Ethernet LAN typically uses coaxial cable or special grades of twisted pair wires. Devices are connected to the cable and compete for access using a CSMA/CD protocol.
## Event
An event is an observable occurrence in a system or network.
## Exponential Backoff Algorithm
An exponential backoff algorithm is used to adjust TCP timeout values on the fly so that network devices do not continue to timeout sending data over saturated links.
## Exposure
A threat action whereby sensitive data is directly released to an unauthorized entity.
## Extended ACLs (Cisco)
Extended ACLs are a more powerful form of Standard ACLs on Cisco routers. They can make filtering decisions based on IP addresses (source or destination), Ports (source or destination), protocols, and whether a session is established.
## Extensible Authentication Protocol (EAP)
A framework that supports multiple, optional authentication mechanisms for PPP, including clear-text passwords, challenge-response, and arbitrary dialog sequences.
## Exterior Gateway Protocol (EGP)
A protocol which distributes routing information to the routers which connect autonomous systems.
