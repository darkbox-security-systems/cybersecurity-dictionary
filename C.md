# C

## Cache
Pronounced cash, a special high-speed storage mechanism. It can be either a reserved section of main memory or an independent high-speed storage device. Two types of caching are commonly used in personal computers: memory caching and disk caching.
## Cache Cramming
Cache Cramming is the technique of tricking a browser to run cached Java code from the local disk, instead of the internet zone, so it runs with less restrictive permissions.
## Cache Poisoning
Malicious or misleading data from a remote name server is saved [cached] by another name server. Typically used with DNS cache poisoning attacks.
## Call Admission Control (CAC)
The inspection and control all inbound and outbound voice network activity by a voice firewall based on user-defined policies.
## Cell
A cell is a unit of data transmitted over an ATM network.
## Certificate-Based Authentication
Certificate-Based Authentication is the use of SSL and certificates to authenticate and encrypt HTTP traffic.
## CGI
Common Gateway Interface. This mechanism is used by HTTP servers (web servers) to pass parameters to executable scripts in order to generate responses dynamically.
## Chain of Custody
Chain of Custody is the important application of the Federal rules of evidence and its handling.
## Challenge-Handshake Authentication Protocol (CHAP)
The Challenge-Handshake Authentication Protocol uses a challenge/response authentication mechanism where the response varies every challenge to prevent replay attacks.
## Checksum
A value that is computed by a function that is dependent on the contents of a data object and is stored or transmitted together with the object, for the purpose of detecting changes in the data.
## Cipher
A cryptographic algorithm for encryption and decryption.
## Ciphertext
Ciphertext is the encrypted form of the message being sent.
## Circuit Switched Network
A circuit switched network is where a single continuous physical circuit connected two endpoints where the route was immutable once set up.
## Client
A system entity that requests and uses a service provided by another system entity, called a "server." In some cases, the server may itself be a client of some other server.
## Cold/Warm/Hot Disaster Recovery Site
* Hot site. It contains fully redundant hardware and software, with telecommunications, telephone and utility connectivity to continue all primary site operations. Failover occurs within minutes or hours, following a disaster. Daily data synchronization usually occurs between the primary and hot site, resulting in minimum or no data loss. Offsite data backup tapes might be obtained and delivered to the hot site to help restore operations. Backup tapes should be regularly tested to detect data corruption, malicious code, and environmental damage. A hot site is the most expensive option. 
* Warm site. It contains partially redundant hardware and software, with telecommunications, telephone and utility connectivity to continue some, but not all primary site operations. Failover occurs within hours or days, following a disaster. Daily or weekly data synchronization usually occurs between the primary and warm site, resulting in minimum data loss. Offsite data backup tapes must be obtained and delivered to the warm site to restore operations. A warm site is the second most expensive option.
* Cold site. Hardware is ordered, shipped, and installed, and software is loaded. Basic telecommunications, telephone and utility connectivity might need turning on to continue some, but not all primary site operations. Relocation occurs within weeks or longer, depending on hardware arrival time, following a disaster. No data synchronization occurs between the primary and cold site, and could result in significant data loss. Offsite data backup tapes must be obtained and delivered to the cold site to restore operations. A cold site is the least expensive option.
## Collision
A collision occurs when multiple systems transmit simultaneously on the same wire.
## Competitive Intelligence
Competitive Intelligence is espionage using legal, or at least not obviously illegal, means.
## Computer Emergency Response Team (CERT)
An organization that studies computer and network INFOSEC in order to provide incident response services to victims of attacks, publish alerts concerning vulnerabilities and threats, and offer other information to help improve computer and network security.
## Computer Network
A collection of host computers together with the sub-network or inter-network through which they can exchange data.
## Confidentiality
Confidentiality is the need to ensure that information is disclosed only to those who are authorized to view it.
## Configuration Management
Establish a known baseline condition and manage it.
## Cookie
Data exchanged between an HTTP server and a browser (a client of the server) to store state information on the client side and retrieve it later for server use. An HTTP server, when sending data to a client, may send along a cookie, which the client retains after the HTTP connection closes. A server can use this mechanism to maintain persistent client-side state information for HTTP-based applications, retrieving the state information in later connections.
## Corruption
A threat action that undesirably alters system operation by adversely modifying system functions or data.
## Cost Benefit Analysis
A cost benefit analysis compares the cost of implementing countermeasures with the value of the reduced risk.
## Countermeasure
Reactive methods used to prevent an exploit from successfully occurring once a threat has been detected. Intrusion Prevention Systems (IPS) commonly employ countermeasures to prevent intruders form gaining further access to a computer network. Other counter measures are patches, access control lists and malware filters.
## Covert Channels
Covert Channels are the means by which information can be communicated between two parties in a covert fashion using normal system operations. For example by changing the amount of hard drive space that is available on a file server can be used to communicate information.
## Crimeware
A type of malware used by cyber criminals. The malware is designed to enable the cybercriminal to make money off of the infected system (such as harvesting key strokes, using the infected systems to launch Denial of Service Attacks, etc.).
## Cron
Cron is a Unix application that runs jobs for users and administrators at scheduled times of the day.
## Crossover Cable
A crossover cable reverses the pairs of cables at the other end and can be used to connect devices directly together.
## Cryptanalysis
The mathematical science that deals with analysis of a cryptographic system in order to gain knowledge needed to break or circumvent the protection that the system is designed to provide. In other words, convert the cipher text to plaintext without knowing the key.
## Cryptographic Algorithm or Hash
An algorithm that employs the science of cryptography, including encryption algorithms, cryptographic hash algorithms, digital signature algorithms, and key agreement algorithms.
## Cut-Through
Cut-Through is a method of switching where only the header of a packet is read before it is forwarded to its destination.
## Cybercrime
A crime committed using computer networks.
## Cybersecurity
Actions taken protect against physical and electronic attacks.
## Cyclic Redundancy Check (CRC)
Sometimes called "cyclic redundancy code." A type of checksum algorithm that is not a cryptographic hash but is used to implement data integrity service where accidental changes to data are expected.
