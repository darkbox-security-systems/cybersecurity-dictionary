#P

## Packet
A piece of a message transmitted over a packet-switching network. One of the key features of a packet is that it contains the destination address in addition to the data. In IP networks, packets are often called datagrams.
## Packet Switched Network
A packet switched network is where individual packets each follow their own paths through the network from one endpoint to another.
## Partitions
Major divisions of the total physical hard disk space.
## Password Authentication Protocol (PAP)
Password Authentication Protocol is a simple, weak authentication mechanism where a user enters the password and it is then sent across the network, usually in the clear.
## Password Cracking
Password cracking is the process of attempting to guess passwords, given the password file information.
## Password Sniffing
Passive wiretapping, usually on a local area network, to gain knowledge of passwords.
## Patch
A patch is a small update released by a software manufacturer to fix bugs in existing programs.
## Patching
Patching is the process of updating software to a different version.
## Payload
Payload is the actual application data a packet contains.
## Penetration
Gaining unauthorized logical access to sensitive data by circumventing a system's protections.
## Penetration Testing
Penetration testing is used to test the external perimeter security of a network or facility.
## Permutation
Permutation keeps the same letters but changes the position within a text to scramble the message.
## Personal Firewalls
Personal firewalls are those firewalls that are installed and run on individual PCs.
## Pharming
This is a more sophisticated form of MITM attack. A user’s session is redirected to a masquerading website. This can be achieved by corrupting a DNS server on the Internet and pointing a URL to the masquerading website’s IP. Almost all users use a URL like www.worldbank.com instead of the real IP (192.86.99.140) of the website. Changing the pointers on a DNS server, the URL can be redirected to send traffic to the IP of the pseudo website. At the pseudo website, transactions can be mimicked and information like login credentials can be gathered. With this the attacker can access the real www.worldbank.com site and conduct transactions using the credentials of a valid user on that website.
## Phishing
The use of e-mails that appear to originate from a trusted source to trick a user into entering valid credentials at a fake website. Typically the e-mail and the web site looks like they are part of a bank the user is doing business with.
## Ping of Death
An attack that sends an improperly large ICMP echo request packet (a "ping") with the intent of overflowing the input buffers of the destination machine and causing it to crash.
## Ping Scan
A ping scan looks for machines that are responding to ICMP Echo Requests.
## Ping Sweep
An attack that sends ICMP echo requests ("pings") to a range of IP addresses, with the goal of finding hosts that can be probed for vulnerabilities.
## Plaintext
Ordinary readable text before being encrypted into ciphertext or after being decrypted.
## Point-to-Point Protocol (PPP)
A protocol for communication between two computers using a serial interface, typically a personal computer connected by phone line to a server. It packages your computer's TCP/IP packets and forwards them to the server where they can actually be put on the Internet.
## Point-to-Point Tunneling Protocol (PPTP)
A protocol (set of communication rules) that allows corporations to extend their own corporate network through private "tunnels" over the public Internet.
## Poison Reverse
Split horizon with poisoned reverse (more simply, poison reverse) does include such routes in updates, but sets their metrics to infinity. In effect, advertising the fact that there routes are not reachable.
## Polyinstantiation
Polyinstantiation is the ability of a database to maintain multiple records with the same key. It is used to prevent inference attacks.
## Polymorphism
Polymorphism is the process by which malicious software changes its underlying code to avoid detection.
## Port
A port is nothing more than an integer that uniquely identifies an endpoint of a communication stream. Only one process per machine can listen on the same port number.
## Port Scan
A port scan is a series of messages sent by someone attempting to break into a computer to learn which computer network services, each associated with a "well-known" port number, the computer provides. Port scanning, a favorite approach of computer cracker, gives the assailant an idea where to probe for weaknesses. Essentially, a port scan consists of sending a message to each port, one at a time. The kind of response received indicates whether the port is used and can therefore be probed for weakness.
## Possession
Possession is the holding, control, and ability to use information.
## Post Office Protocol, Version 3 (POP3)
An Internet Standard protocol by which a client workstation can dynamically access a mailbox on a server host to retrieve mail messages that the server has received and is holding for the client.
## Practical Extraction and Reporting Language (Perl)
A script programming language that is similar in syntax to the C language and that includes a number of popular Unix facilities such as sed, awk, and tr.
## Preamble
A preamble is a signal used in network communications to synchronize the transmission timing between two or more systems. Proper timing ensures that all systems are interpreting the start of the information transfer correctly. A preamble defines a specific series of transmission pulses that is understood by communicating systems to mean "someone is about to transmit data". This ensures that systems receiving the information correctly interpret when the data transmission starts. The actual pulses used as a preamble vary depending on the network communication technology in use.
## Pretty Good Privacy (PGP)TM
Trademark of Network Associates, Inc., referring to a computer program (and related protocols) that uses cryptography to provide data security for electronic mail and other applications on the Internet.
## Private Addressing
IANA has set aside three address ranges for use by private or non-Internet connected networks. This is referred to as Private Address Space and is defined in RFC 1918. The reserved address blocks are: 10.0.0.0 to 10.255.255.255 (10/8 prefix) 172.16.0.0 to 172.31.255.255 (172.16/12 prefix) 192.168.0.0 to 192.168.255.255 (192.168/16 prefix)
## Program Infector
A program infector is a piece of malware that attaches itself to existing program files.
## Program Policy
A program policy is a high-level policy that sets the overall tone of an organization's security approach.
## Promiscuous Mode
When a machine reads all packets off the network, regardless of who they are addressed to. This is used by network administrators to diagnose network problems, but also by unsavory characters who are trying to eavesdrop on network traffic (which might contain passwords or other information).
## Proprietary Information
Proprietary information is that information unique to a company and its ability to compete, such as customer lists, technical data, product costs, and trade secrets.
## Protocol
A formal specification for communicating; an IP address the special set of rules that end points in a telecommunication connection use when they communicate. Protocols exist at several levels in a telecommunication connection.
## Protocol Stacks (OSI)
A set of network protocol layers that work together.
## Proxy Server
A server that acts as an intermediary between a workstation user and the Internet so that the enterprise can ensure security, administrative control, and caching service. A proxy server is associated with or part of a gateway server that separates the enterprise network from the outside network and a firewall server that protects the enterprise network from outside intrusion.
## Public Key
The publicly disclosed component of a pair of cryptographic keys used for asymmetric cryptography.
## Public Key Encryption
The popular synonym for "asymmetric cryptography".
## Public Key Infrastructure (PKI)
A PKI (public key infrastructure) enables users of a basically unsecured public network such as the Internet to securely and privately exchange data and money through the use of a public and a private cryptographic key pair that is obtained and shared through a trusted authority. The public key infrastructure provides for a digital certificate that can identify an individual or an organization and directory services that can store and, when necessary, revoke the certificates.
## Public-Key Forward Secrecy (PFS)
For a key agreement protocol based on asymmetric cryptography, the property that ensures that a session key derived from a set of long-term public and private keys will not be compromised if one of the private keys is compromised in the future.
