# S

## S/Key
A security mechanism that uses a cryptographic hash function to generate a sequence of 64-bit, one-time passwords for remote user login. The client generates a one-time password by applying the MD4 cryptographic hash function multiple times to the user's secret key. For each successive authentication of the user, the number of hash applications is reduced by one.
## Safety
Safety is the need to ensure that the people involved with the company, including employees, customers, and visitors, are protected from harm.
## Scavenging
Searching through data residue in a system to gain unauthorized knowledge of sensitive data.
## Script Virus
A virus that is hidden on a website and attach to a specific page to that when the link is visited the script will be executed on the visitor’s computer.
## Secure Electronic Transactions (SET)
Secure Electronic Transactions is a protocol developed for credit card transactions in which all parties (customers, merchant, and bank) are authenticated using digital signatures, encryption protects the message and provides integrity, and provides end-to-end security for credit card transactions online.
## Secure Shell (SSH)
A program to log into another computer over a network, to execute commands in a remote machine, and to move files from one machine to another.
## Secure Sockets Layer (SSL)
A protocol developed by Netscape for transmitting private documents via the Internet. SSL works by using a public key to encrypt data that's transferred over the SSL connection.
## Security Policy
A set of rules and practices that specify or regulate how a system or organization provides security services to protect sensitive and critical system resources.
## Segment
Segment is another name for TCP packets.
## Sensitive Information
Sensitive information, as defined by the federal government, is any unclassified information that, if compromised, could adversely affect the national interest, or conduct of federal initiatives.
## Separation of Duties
Separation of duties is the principle of splitting privileges among multiple individuals or systems.
## Server
A system entity that provides a service in response to requests from other system entities called clients.
## Session
A session is a virtual connection between two hosts by which network traffic is passed.
## Session Hijacking
Take over a session that someone else has established.
## Session Key
In the context of symmetric encryption, a key that is temporary or is used for a relatively short period of time. Usually, a session key is used for a defined period of communication between two computers, such as for the duration of a single connection or transaction set, or the key is used in an application that protects relatively large amounts of data and, therefore, needs to be re-keyed frequently.
## SHA1
A one way cryptographic hash function. Also see "MD5"
## Shadow Password Files
A system file in which encryption user password are stored so that they aren't available to people who try to break into the system.
## Share
A share is a resource made public on a machine, such as a directory (file share) or printer (printer share).
## Shell
A Unix term for the interactive user interface with an operating system. The shell is the layer of programming that understands and executes the commands a user enters. In some systems, the shell is called a command interpreter. A shell usually implies an interface with a command syntax (think of the DOS operating system and its "C:>" prompts and user commands such as "dir" and "edit").
## Signals Analysis
Gaining indirect knowledge of communicated data by monitoring and analyzing a signal that is emitted by a system and that contains the data but is not intended to communicate the data.
## Signature
A Signature is a distinct pattern in network traffic that can be identified to a specific tool or exploit.
## Simple Integrity Property
In Simple Integrity Property a user cannot write data to a higher integrity level than their own.
## Simple Network Management Protocol (SNMP)
The protocol governing network management and the monitoring of network devices and their functions. A set of protocols for managing complex networks.
## Simple Security Property
In Simple Security Property a user cannot read data of a higher classification than their own.
## Simple Mail Transfer Protocol (SMTP)
The standard protocol for email services on a TCP/IP network. SMTP provides the ability to send and receive email messages. SMTP is an application-layer protocol that enables the transmission and delivery of email over the Internet. SMTP is created and maintained by the Internet Engineering Task Force (IETF).
## Smartcard
A smartcard is an electronic badge that includes a magnetic strip or chip that can record and replay a set key.
## Smurf
The Smurf attack works by spoofing the target address and sending a ping to the broadcast address for a remote network, which results in a large amount of ping replies being sent to the target.
## Sniffer
A sniffer is a tool that monitors network traffic as it received in a network interface.
## Sniffing
A synonym for "passive wiretapping."
## Social Engineering
A euphemism for non-technical or low-technology means - such as lies, impersonation, tricks, bribes, blackmail, and threats - used to attack information systems.
## Socket
The socket tells a host's IP stack where to plug in a data stream so that it connects to the right application.
## Socket Pair
A way to uniquely specify a connection, i.e., source IP address, source port, destination IP address, destination port.
## SOCKS
A protocol that a proxy server can use to accept requests from client users in a company's network so that it can forward them across the Internet. SOCKS uses sockets to represent and keep track of individual connections. The client side of SOCKS is built into certain Web browsers and the server side can be added to a proxy server.
## Software
Computer programs (which are stored in and executed by computer hardware) and associated data (which also is stored in the hardware) that may be dynamically written or modified during execution.
## Source Port
The port that a host uses to connect to a server. It is usually a number greater than or equal to 1024. It is randomly generated and is different each time a connection is made.
## Spam
Electronic junk mail or junk newsgroup postings.
## Spanning Port
Configures the switch to behave like a hub for a specific port.
## Split Horizon
Split horizon is a algorithm for avoiding problems caused by including routes in updates sent to the gateway from which they were learned.
## Split Key
A cryptographic key that is divided into two or more separate data items that individually convey no knowledge of the whole key that results from combining the items.
## Spoof
Attempt by an unauthorized entity to gain access to a system by posing as an authorized user.
## SQL Injection
SQL injection is a type of input validation attack specific to database-driven applications where SQL code is inserted into application queries to manipulate the database.
## Stack Mashing
Stack mashing is the technique of using a buffer overflow to trick a computer into executing arbitrary code.
## Standard ACLs (Cisco)
Standard ACLs on Cisco routers make packet filtering decisions based on Source IP address only.
## Star Property
In Star Property, a user cannot write data to a lower classification level without logging in at that lower classification level.
## State Machine
A system that moves through a series of progressive conditions.
## Stateful Inspection
Also referred to as dynamic packet filtering. Stateful inspection is a firewall architecture that works at the network layer. Unlike static packet filtering, which examines a packet based on the information in its header, stateful inspection examines not just the header information but also the contents of the packet up through the application layer in order to determine more about the packet than just information about its source and destination.
## Static Host Tables
Static host tables are text files that contain hostname and address mapping.
## Static Routing
Static routing means that routing table entries contain information that does not change.
## Stealthing
Stealthing is a term that refers to approaches used by malicious code to conceal its presence on the infected system.
## Steganalysis
Steganalysis is the process of detecting and defeating the use of steganography.
## Steganography
Methods of hiding the existence of a message or other data. This is different than cryptography, which hides the meaning of a message but does not hide the message itself. An example of a steganographic method is "invisible" ink.
## Stimulus
Stimulus is network traffic that initiates a connection or solicits a response.
## Store-and-Forward
Store-and-Forward is a method of switching where the entire packet is read by a switch to determine if it is intact before forwarding it.
## Straight-Through Cable
A straight-through cable is where the pins on one side of the connector are wired to the same pins on the other end. It is used for interconnecting nodes on the network.
## Stream Cipher
A stream cipher works by encryption a message a single bit, byte, or computer word at a time.
## Strong Star Property
In Strong Star Property, a user cannot write data to higher or lower classifications levels than their own.
## Sub Network
A separately identifiable part of a larger network that typically represents a certain limited number of host computers, the hosts in a building or geographic area, or the hosts on an individual local area network.
## Subnet Mask
A subnet mask (or number) is used to determine the number of bits used for the subnet and host portions of the address. The mask is a 32-bit value that uses one-bits for the network and subnet portions and zero-bits for the host portion.
## Switch
A switch is a networking device that keeps track of MAC addresses attached to each of its ports so that data is only transmitted on the ports that are the intended recipient of the data.
## Switched Network
A communications network, such as the public switched telephone network, in which any user may be connected to any other user through the use of message, circuit, or packet switching and control devices. Any network providing switched communications service.
## Symbolic Links
Special files which point at another file.
## Symmetric Cryptography
A branch of cryptography involving algorithms that use the same key for two different steps of the algorithm (such as encryption and decryption, or signature creation and signature verification). Symmetric cryptography is sometimes called "secret-key cryptography" (versus public-key cryptography) because the entities that share the key.
## Symmetric Key
A cryptographic key that is used in a symmetric cryptographic algorithm.
## SYN Flood
A denial of service attack that sends a host more TCP SYN packets (request to synchronize sequence numbers, used when opening a connection) than the protocol implementation can handle.
## Synchronization
Synchronization is the signal made up of a distinctive pattern of bits that network hardware looks for to signal that start of a frame.
## Syslog
Syslog is the system logging facility for Unix systems.
## System Security Officer (SSO)
A person responsible for enforcement or administration of the security policy that applies to the system.
## System-Specific Policy
A System-specific policy is a policy written for a specific system or device.
