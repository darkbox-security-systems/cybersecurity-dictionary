# I

## Identity
Identity is whom someone or what something is, for example, the name by which something is known.
## Incident
An incident as an adverse network event in an information system or network or the threat of the occurrence of such an event.
## Incident Handling
Incident Handling is an action plan for dealing with intrusions, cyber-theft, denial of service, fire, floods, and other security-related events. It is comprised of a six step process: Preparation, Identification, Containment, Eradication, Recovery, and Lessons Learned.
## Incremental Backups
Incremental backups only backup the files that have been modified since the last backup. If dump levels are used, incremental backups only backup files changed since last backup of a lower dump level.
## Inetd (xinetd)
Inetd (or Internet Daemon) is an application that controls smaller internet services like telnet, ftp, and POP.
## Inference Attack
Inference Attacks rely on the user to make logical connections between seemingly unrelated pieces of information.
## Information Warfare
Information Warfare is the competition between offensive and defensive players over information resources.
## Ingress Filtering
Ingress Filtering is filtering inbound traffic.
## Input Validation Attacks
Input Validations Attacks are where an attacker intentionally sends unusual input in the hopes of confusing an application.
## Integrity
Integrity is the need to ensure that information has not been changed accidentally or deliberately, and that it is accurate and complete.
## Integrity Star Property
In Integrity Star Property a user cannot read data of a lower integrity level then their own.
## Internet
A term to describe connecting multiple separate networks together.
## Internet Control Message Protocol (ICMP)
An Internet Standard protocol that is used to report error conditions during IP datagram processing and to exchange other information concerning the state of the IP network.
## Internet Engineering Task Force (IETF)
The body that defines standard Internet operating protocols such as TCP/IP. The IETF is supervised by the Internet Society Internet Architecture Board (IAB). IETF members are drawn from the Internet Society's individual and organization membership.
## Internet Message Access Protocol (IMAP)
A protocol that defines how a client should fetch mail from and return mail to a mail server. IMAP is intended as a replacement for or extension to the Post Office Protocol (POP). It is defined in RFC 1203 (v3) and RFC 2060 (v4).
## Internet Protocol (IP)
The method or protocol by which data is sent from one computer to another on the Internet.
## Internet Protocol Security (IPsec)
A developing standard for security at the network or packet processing layer of network communication.
## Internet Standard
A specification, approved by the IESG and published as an RFC, that is stable and well-understood, is technically competent, has multiple, independent, and interoperable implementations with substantial operational experience, enjoys significant public support, and is recognizably useful in some or all parts of the Internet.
## Interrupt
An Interrupt is a signal that informs the OS that something has occurred.
## Intranet
A computer network, especially one based on Internet technology, that an organization uses for its own internal, and usually private, purposes and that is closed to outsiders.
## Intrusion Detection
A security management system for computers and networks. An IDS gathers and analyzes information from various areas within a computer or a network to identify possible security breaches, which include both intrusions (attacks from outside the organization) and misuse (attacks from within the organization).
## IP Address
A computer's inter-network address that is assigned for use by the Internet Protocol and other protocols. An IP version 4 address is written as a series of four 8-bit numbers separated by periods.
## IP Flood
A denial of service attack that sends a host more echo request ("ping") packets than the protocol implementation can handle.
## IP Forwarding
IP forwarding is an Operating System option that allows a host to act as a router. A system that has more than 1 network interface card must have IP forwarding turned on in order for the system to be able to act as a router.
## IP Spoofing
The technique of supplying a false IP address.
## ISO
International Organization for Standardization, a voluntary, non-treaty, non-government organization, established in 1947, with voting members that are designated standards bodies of participating nations and non-voting observer organizations.
## Issue-Specific Policy
An Issue-Specific Policy is intended to address specific needs within an organization, such as a password policy.
## ITU-T
International Telecommunications Union, Telecommunication Standardization Sector (formerly "CCITT"), a United Nations treaty organization that is composed mainly of postal, telephone, and telegraph authorities of the member countries and that publishes standards called "Recommendations."
