# A

## Access Control
Access Control ensures that resources are only granted to those users who are entitled to them.
## Access Control List (ACL)
A mechanism that implements access control for a system resource by listing the identities of the system entities that are permitted to access the resource.
## Access Control Service
A security service that provides protection of system resources against unauthorized access. The two basic mechanisms for implementing this service are ACLs and tickets.
## Access Management Access
Management is the maintenance of access information which consists of four tasks: account administration, maintenance, monitoring, and revocation.
## Access Matrix
An Access Matrix uses rows to represent subjects and columns to represent objects with privileges listed in each cell.
## Account Harvesting
Account Harvesting is the process of collecting all the legitimate account names on a system.
## ACK Piggybacking
ACK piggybacking is the practice of sending an ACK inside another packet going to the same destination.
## Active Content
Program code embedded in the contents of a web page. When the page is accessed by a web browser, the embedded code is automatically downloaded and executed on the user's workstation. Ex. Java, ActiveX (MS)
## Activity Monitors
Activity monitors aim to prevent virus infection by monitoring for malicious activity on a system, and blocking that activity when possible.
## Address Resolution Protocol (ARP)
Address Resolution Protocol (ARP) is a protocol for mapping an Internet Protocol address to a physical machine address that is recognized in the local network. A table, usually called the ARP cache, is used to maintain a correlation between each MAC address and its corresponding IP address. ARP provides the protocol rules for making this correlation and providing address conversion in both directions.
## Advanced Encryption Standard (AES)
An encryption standard being developed by NIST. Intended to specify an unclassified, publicly-disclosed, symmetric encryption algorithm.
## Advanced Persistent Threat
An adversary that possesses sophisticated levels of expertise and significant resources which allow it to create opportunities to achieve its objectives by using multiple attack vectors (e.g., cyber, physical, and deception).
## Algorithm
A finite set of step-by-step instructions for a problem-solving or computation procedure, especially one that can be implemented by a computer.
## Applet
Java programs; an application program that uses the client's web browser to provide a user interface.
## ARPANET
Advanced Research Projects Agency Network, a pioneer packet-switched network that was built in the early 1970s under contract to the US Government, led to the development of today's Internet, and was decommissioned in June 1990.
## Asymmetric Cryptography
Public-key cryptography; A modern branch of cryptography in which the algorithms employ a pair of keys (a public key and a private key) and use a different component of the pair for different steps of the algorithm.
## Asymmetric Warfare
Asymmetric warfare is the fact that a small investment, properly leveraged, can yield incredible results.
## Auditing
Auditing is the information gathering and analysis of assets to ensure such things as policy compliance and security from vulnerabilities.
## Authentication
Authentication is the process of confirming the correctness of the claimed identity.
## Authenticity
Authenticity is the validity and conformance of the original information.
## Authorization
Authorization is the approval, permission, or empowerment for someone or something to do something.
## Autonomous System
One network or series of networks that are all under one administrative control. An autonomous system is also sometimes referred to as a routing domain. An autonomous system is assigned a globally unique number, sometimes called an Autonomous System Number (ASN).
## Availability
Availability is the need to ensure that the business purpose of the system can be met and that it is accessible to those who need to use it.
