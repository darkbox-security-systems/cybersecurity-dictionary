# L

## Lattice Techniques
Lattice Techniques use security designations to determine access to information.
## Layer 2 Forwarding Protocol (L2F)
An Internet protocol (originally developed by Cisco Corporation) that uses tunneling of PPP over IP to create a virtual extension of a dial-up link across a network, initiated by the dial-up server and transparent to the dial-up user.
## Layer 2 Tunneling Protocol (L2TP)
An extension of the Point-to-Point Tunneling Protocol used by an Internet service provider to enable the operation of a virtual private network over the Internet.
## Least Privilege
Least Privilege is the principle of allowing users or applications the least amount of permissions necessary to perform their intended function.
## Legion
Software to detect unprotected shares.
## Lightweight Directory Access Protocol (LDAP)
A software protocol for enabling anyone to locate organizations, individuals, and other resources such as files and devices in a network, whether on the public Internet or on a corporate Intranet.
## Link State
With link state, routes maintain information about all routers and router-to-router links within a geographic area, and creates a table of best routes with that information.
## Linux
A family of open-source Unix-like operating systems based on the Linux kernel, an operating system kernel first released on September 17, 1991, by Linus Torvalds. Linux is typically packaged in a Linux distribution.
## List Based Access Control
List Based Access Control associates a list of users and their privileges with each object.
## Loadable Kernel Modules (LKM)
Loadable Kernel Modules allow for the adding of additional functionality directly into the kernel while the system is running.
## Log Clipping
Log clipping is the selective removal of log entries from a system log to hide a compromise.
## Logic bombs
Logic bombs are programs or snippets of code that execute when a certain predefined event occurs. Logic bombs may also be set to go off on a certain date or when a specified set of circumstances occurs.
## Logic Gate
A logic gate is an elementary building block of a digital circuit. Most logic gates have two inputs and one output. As digital circuits can only understand binary, inputs and outputs can assume only one of two states, 0 or 1.
## Loopback Address
The loopback address (127.0.0.1) is a pseudo IP address that always refer back to the local host and are never sent out onto a network.
