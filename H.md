# H

## Hardening
Hardening is the process of identifying and fixing vulnerabilities on a system.
## Hardware Token
A small device, typically in the general form factor of a credit card or keychain fob. The simplest hardware tokens look identical to a USB flash drive and contain a small amount of storage holding a certificate or unique identifier and are often called dongles. More complex hardware tokens incorporate LCD displays, keypads for entering passwords, biometric readers, wireless devices, and additional features to enhance security.
## Hash Function
An algorithm that computes a value based on a data object thereby mapping the data object to a smaller data object.
## Hash Functions
(cryptographic) hash functions are used to generate a one way "check sum" for a larger text, which is not trivially reversed. The result of this hash function can be used to validate if a larger file has been altered, without having to compare the larger files to each other. Frequently used hash functions are MD5 and SHA1.
## Header
A header is the extra information in a packet that is needed for the protocol stack to process the packet.
## Hijack Attack
A form of active wiretapping in which the attacker seizes control of a previously established communication association.
## Honey Client
see Honeymonkey.
## Honey pot
Programs that simulate one or more network services that you designate on your computer's ports. An attacker assumes you are running vulnerable services that can be used to break into the machine. A honey pot can be used to log access attempts to those ports including the attacker's keystrokes. This could give you advanced warning of a more concerted attack.
## Honey net
An entire computer network that serves as a honey pot or trap for potential attackers.
## Honeymonkey
Automated system simulating a user browsing websites. The system is typically configured to detect web sites which exploit vulnerabilities in the browser. Also known as Honey Client.
## Hops
A hop is each exchange with a gateway a packet takes on its way to the destination.
## Host
Any computer that has full two-way access to other computers on the Internet. Or a computer with a web server that serves the pages for one or more Web sites.
## Host-Based ID
Host-based intrusion detection systems use information from the operating system audit records to watch all operations occurring on the host that the intrusion detection software has been installed upon. These operations are then compared with a pre-defined security policy. This analysis of the audit trail imposes potentially significant overhead requirements on the system because of the increased amount of processing power which must be utilized by the intrusion detection system. Depending on the size of the audit trail and the processing ability of the system, the review of audit data could result in the loss of a real-time analysis capability.
## HTTP Proxy
An HTTP Proxy is a server that acts as a middleman in the communication between HTTP clients and servers.
## HTTPS
When used in the first part of a URL (the part that precedes the colon and specifies an access scheme or protocol), this term specifies the use of HTTP enhanced by a security mechanism, which is usually SSL.
## Hub
A hub is a network device that operates by repeating data that it receives on one port to all the other ports. As a result, data transmitted by one host is retransmitted to all other hosts on the hub.
## Hybrid Attack
A Hybrid Attack builds on the dictionary attack method by adding numerals and symbols to dictionary words.
## Hybrid Encryption
An application of cryptography that combines two or more encryption algorithms, particularly a combination of symmetric and asymmetric encryption.
## Hyperlink
In hypertext or hypermedia, an information object (such as a word, a phrase, or an image; usually highlighted by color or underscoring) that points (indicates how to connect) to related information that is located elsewhere and can be retrieved by activating the link.
## Hypertext Markup Language (HTML)
The set of markup symbols or codes inserted in a file intended for display on a World Wide Web browser page.
## Hypertext Transfer Protocol (HTTP)
The protocol in the Internet Protocol (IP) family used to transport hypertext documents across an internet.
