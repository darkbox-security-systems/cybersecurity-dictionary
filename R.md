# R

## Race Condition
A race condition exploits the small window of time between a security control being applied and when the service is used.
## Radiation Monitoring
Radiation monitoring is the process of receiving images, data, or audio from an unprotected source by listening to radiation signals.
## Ransomware
A type of malware that is a form of extortion. It works by encrypting a victim's hard drive denying them access to key files. The victim must then pay a ransom to decrypt the files and gain access to them again.
## Reconnaissance
Reconnaissance is the phase of an attack where an attacker finds new systems, maps out networks, and probes for specific, exploitable vulnerabilities.
## Reflexive ACLs (Cisco)
Reflexive ACLs for Cisco routers are a step towards making the router act like a stateful firewall. The router will make filtering decisions based on whether connections are a part of established traffic or not.
## Registry
The Registry in Windows operating systems in the central set of settings and information required to run the Windows computer.
## regression analysis
The use of scripted tests which are used to test software for all possible input is should expect. Typically, developers will create a set of regression tests that are executed before a new version of a software is released. Also see "fuzzing".
## Request for Comment (RFC)
A series of notes about the Internet, started in 1969 (when the Internet was the ARPANET). An Internet Document can be submitted to the IETF by anyone, but the IETF decides if the document becomes an RFC. Eventually, if it gains enough interest, it may evolve into an Internet standard.
## Resource Exhaustion
Resource exhaustion attacks involve tying up finite resources on a system, making them unavailable to others.
## Response
A response is information sent that is responding to some stimulus.
## Reverse Address Resolution Protocol (RARP)
RARP (Reverse Address Resolution Protocol) is a protocol by which a physical machine in a local area network can request to learn its IP address from a gateway server's Address Resolution Protocol table or cache. A network administrator creates a table in a local area network's gateway router that maps the physical machine (or Media Access Control - MAC address) addresses to corresponding Internet Protocol addresses. When a new machine is set up, its RARP client program requests from the RARP server on the router to be sent its IP address. Assuming that an entry has been set up in the router table, the RARP server will return the IP address to the machine which can store it for future use.
## Reverse Engineering
Acquiring sensitive data by disassembling and analyzing the design of a system component.
## Reverse Lookup
Find out the hostname that corresponds to a particular IP address. Reverse lookup uses an IP (Internet Protocol) address to find a domain name.
## Reverse Proxy
Reverse proxies take public HTTP requests and pass them to back-end webservers to send the content to it, so the proxy can then send the content to the end-user.
## Risk
Risk is the product of the level of threat with the level of vulnerability. It establishes the likelihood of a successful attack.
## Risk Assessment
A Risk Assessment is the process by which risks are identified and the impact of those risks determined.
## Risk Averse
Avoiding risk even if this leads to the loss of opportunity. For example, using a (more expensive) phone call vs. sending an e-mail in order to avoid risks associated with e-mail may be considered "Risk Averse"
## Rivest-Shamir-Adleman (RSA)
An algorithm for asymmetric cryptography, invented in 1977 by Ron Rivest, Adi Shamir, and Leonard Adleman.
## Role Based Access Control
Role based access control assigns users to roles based on their organizational functions and determines authorization based on those roles.
## Root
Root is the name of the administrator account in Unix systems.
## Rootkit
A collection of tools (programs) that a hacker uses to mask intrusion and obtain administrator-level access to a computer or computer network.
## Router
Routers interconnect logical networks by forwarding information to other networks based upon IP addresses.
## Routing Information Protocol (RIP)
Routing Information Protocol is a distance vector protocol used for interior gateway routing which uses hop count as the sole metric of a path's cost.
## Routing Loop
A routing loop is where two or more poorly configured routers repeatedly exchange the same packet over and over.
## RPC Scans
RPC scans determine which RPC services are running on a machine.
## Rule Set Based Access Control (RSBAC)
Rule Set Based Access Control targets actions based on rules for entities operating on objects.
