# D

## Daemon
A program which is often started at the time the system boots and runs continuously without intervention from any of the users on the system. The daemon program forwards the requests to other programs (or processes) as appropriate. The term daemon is a Unix term, though many other operating systems provide support for daemons, though they're sometimes called other names. Windows, for example, refers to daemons and System Agents and services.
## Data Aggregation
Data Aggregation is the ability to get a more complete picture of the information by analyzing several different types of records at once.
## Data Custodian
A Data Custodian is the entity currently using or manipulating the data, and therefore, temporarily taking responsibility for the data.
## Data Encryption Standard (DES)
A widely-used method of data encryption using a private (secret) key. There are 72,000,000,000,000,000 (72 quadrillion) or more possible encryption keys that can be used. For each given message, the key is chosen at random from among this enormous number of keys. Like other private key cryptographic methods, both the sender and the receiver must know and use the same private key.
## Data Mining
Data Mining is a technique used to analyze existing information, usually with the intention of pursuing new avenues to pursue business.
## Data Owner
A Data Owner is the entity having responsibility and authority for the data.
## Data Warehousing
Data Warehousing is the consolidation of several previously independent databases into one location.
## Datagram
Request for Comment 1594 says, "a self-contained, independent entity of data carrying sufficient information to be routed from the source to the destination computer without reliance on earlier exchanges between this source and destination computer and the transporting network." The term has been generally replaced by the term packet. Datagrams or packets are the message units that the Internet Protocol deals with and that the Internet transports. A datagram or packet needs to be self-contained without reliance on earlier exchanges because there is no connection of fixed duration between the two communicating points as there is, for example, in most voice telephone conversations. (This kind of protocol is referred to as connectionless.)
## Day Zero
The "Day Zero" or "Zero Day" is the day a new vulnerability is made known. In some cases, a "zero day" exploit is referred to an exploit for which no patch is available yet. ("day one"-> day at which the patch is made available).
## Decapsulation
Decapsulation is the process of stripping off one layer's headers and passing the rest of the packet up to the next higher layer on the protocol stack.
## Decryption
Decryption is the process of transforming an encrypted message into its original plaintext.
## Defacement
Defacement is the method of modifying the content of a website in such a way that it becomes "vandalized" or embarrassing to the website owner.
## Defense In-Depth
Defense In-Depth is the approach of using multiple layers of security to guard against failure of a single security component.
## Demilitarized Zone (DMZ)
In computer security, in general a demilitarized zone (DMZ) or perimeter network is a network area (a subnetwork) that sits between an organization's internal network and an external network, usually the Internet. DMZ's help to enable the layered security model in that they provide subnetwork segmentation based on security requirements or policy. DMZ's provide either a transit mechanism from a secure source to an insecure destination or from an insecure source to a more secure destination. In some cases, a screened subnet which is used for servers accessible from the outside is referred to as a DMZ.
## Denial of Service
The prevention of authorized access to a system resource or the delaying of system operations and functions.
## Dictionary Attack
An attack that tries all of the phrases or words in a dictionary, trying to crack a password or key. A dictionary attack uses a predefined list of words compared to a brute force attack that tries all possible combinations.
## Diffie-Hellman
A key agreement algorithm published in 1976 by Whitfield Diffie and Martin Hellman. Diffie-Hellman does key establishment, not encryption. However, the key that it produces may be used for encryption, for further key management operations, or for any other cryptography.
## Digest Authentication
Digest Authentication allows a web client to compute MD5 hashes of the password to prove it has the password.
## Digital Certificate
A digital certificate is an electronic "credit card" that establishes your credentials when doing business or other transactions on the Web. It is issued by a certification authority. It contains your name, a serial number, expiration dates, a copy of the certificate holder's public key (used for encrypting messages and digital signatures), and the digital signature of the certificate-issuing authority so that a recipient can verify that the certificate is real.
## Digital Envelope
A digital envelope is an encrypted message with the encrypted session key.
## Digital Signature
A digital signature is a hash of a message that uniquely identifies the sender of the message and proves the message hasn't changed since transmission.
## Digital Signature Algorithm (DSA)
An asymmetric cryptographic algorithm that produces a digital signature in the form of a pair of large numbers. The signature is computed using rules and parameters such that the identity of the signer and the integrity of the signed data can be verified.
## Digital Signature Standard (DSS)
The US Government standard that specifies the Digital Signature Algorithm (DSA), which involves asymmetric cryptography.
## Disassembly
The process of taking a binary program and deriving the source code from it.
## Disaster Recovery Plan (DRP)
A Disaster Recovery Plan is the process of recovery of IT systems in the event of a disruption or disaster.
## Discretionary Access Control (DAC)
Discretionary Access Control consists of something the user can manage, such as a document password.
## Disruption
A circumstance or event that interrupts or prevents the correct operation of system services and functions.
## Distance Vector
Distance vectors measure the cost of routes to determine the best route to all known networks.
## Distributed Scans
Distributed Scans are scans that use multiple source addresses to gather information.
## Domain
A sphere of knowledge, or a collection of facts about some program entities or a number of network points or addresses, identified by a name. On the Internet, a domain consists of a set of network addresses. In the Internet's domain name system, a domain is a name with which name server records are associated that describe sub-domains or host. In Windows NT and Windows 2000, a domain is a set of network resources (applications, printers, and so forth) for a group of users. The user need only to log in to the domain to gain access to the resources, which may be located on a number of different servers in the network.
## Domain Hijacking
Domain hijacking is an attack by which an attacker takes over a domain by first blocking access to the domain's DNS server and then putting his own server up in its place.
## Domain Name
A domain name locates an organization or other entity on the Internet. For example, the domain name "www.sans.org" locates an Internet address for "sans.org" at Internet point 199.0.0.2 and a particular host server named "www". The "org" part of the domain name reflects the purpose of the organization or entity (in this example, "organization") and is called the top-level domain name. The "sans" part of the domain name defines the organization or entity and together with the top-level is called the second-level domain name.
## Domain Name System (DNS)
The domain name system (DNS) is the way that Internet domain names are located and translated into Internet Protocol addresses. A domain name is a meaningful and easy-to-remember "handle" for an Internet address.
## Due Care
Due care ensures that a minimal level of protection is in place in accordance with the best practice in the industry.
## Due Diligence
Due diligence is the requirement that organizations must develop and deploy a protection plan to prevent fraud, abuse, and additional deploy a means to detect them if they occur.
## DumpSec
DumpSec is a security tool that dumps a variety of information about a system's users, file system, registry, permissions, password policy, and services.
## Dumpster Diving
Dumpster Diving is obtaining passwords and corporate directories by searching through discarded media.
## Dynamic Link Library
A collection of small programs, any of which can be called when needed by a larger program that is running in the computer. The small program that lets the larger program communicate with a specific device such as a printer or scanner is often packaged as a DLL program (usually referred to as a DLL file).
## Dynamic Routing Protocol
Allows network devices to learn routes. Ex. RIP, EIGRP Dynamic routing occurs when routers talk to adjacent routers, informing each other of what networks each router is currently connected to. The routers must communicate using a routing protocol, of which there are many to choose from. The process on the router that is running the routing protocol, communicating with its neighbor routers, is usually called a routing daemon. The routing daemon updates the kernel's routing table with information it receives from neighbor routers.
