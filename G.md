# G

## Gateway
A network point that acts as an entrance to another network.
## gethostbyaddr
The gethostbyaddr DNS query is when the address of a machine is known and the name is needed.
## gethostbyname
The gethostbyname DNS quest is when the name of a machine is known and the address is needed.
## GNU
GNU is a Unix-like operating system that comes with source code that can be copied, modified, and redistributed. The GNU project was started in 1983 by Richard Stallman and others, who formed the Free Software Foundation.
## Gnutella
An Internet file sharing utility. Gnutella acts as a server for sharing files while simultaneously acting as a client that searches for and downloads files from other users.
