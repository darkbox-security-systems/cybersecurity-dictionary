# Cybersecurity Dictionary

## Description
This is free resource for anyon needing a refrence guide to cybersecurity terms and their meanings. Please feel free to download all files and compile them how you see fit. You can also download the already compiled PDF if you so choose.

## Roadmap
We plan on updating and adding to this as needed since the world of cybersecurity is always changing.

## Contributing
If you are interested in becoming a contributor please email info(at)darkboxsecurity(dot)com with the subject "GitLab Contribution"

## Authors and acknowledgment

The Green Knight

## License
Completely in the public domain and free for anyone to use and distribute.
