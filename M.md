# M

## MAC Address
A physical address: a numeric value that uniquely identifies that network device from every other device on the planet.
## MacroVirus
A virus that is attached to a document such as a Word Document (.doc) or Open Document Text (.odt) file.
## Malicious Code
Software (e.g., Trojan horse) that appears to perform a useful or desirable function, but actually gains unauthorized access to system resources or tricks a user into executing other malicious logic.
## Malware
A generic term for a number of different types of malicious code.
## Mandatory Access Control (MAC)
Mandatory Access Control controls is where the system controls access to resources based on classification levels assigned to both the objects and the users. These controls cannot be changed by anyone.
## Masquerade Attack
A type of attack in which one system entity illegitimately poses as (assumes the identity of) another entity.
## md5
A one way cryptographic hash function. Also see "hash functions" and "sha1"
## Measures of Effectiveness (MOE)
Measures of Effectiveness is a probability model based on engineering concepts that allows one to approximate the impact a give action will have on an environment. In Information warfare it is the ability to attack or defend within an Internet environment.
## Monoculture
Monoculture is the case where a large number of users run the same software, and are vulnerable to the same attacks.
## Morris Worm
A worm program written by Robert T. Morris, Jr. that flooded the ARPANET in November, 1988, causing problems for thousands of hosts.
## Multi-Cast
Broadcasting from one host to a given set of hosts.
## Multi-Homed
You are "multi-homed" if your network is directly connected to two or more ISP's.
## Multiplexing
To combine multiple signals from possibly disparate sources, in order to transmit them over a single path.
