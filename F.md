# F

## False Rejects
False Rejects are when an authentication system fails to recognize a valid user.
## Fast File System
The first major revision to the Unix file system, providing faster read access and faster (delayed, asynchronous) write access through a disk cache and better file system layout on disk. It uses inodes (pointers) and data blocks.
## Fast Flux
Protection method used by botnets consisting of a continuous and fast change of the DNS records for a domain name through different IP addresses.
## Fault Line Attacks
Fault Line Attacks use weaknesses between interfaces of systems to exploit gaps in coverage.
## File Transfer Protocol (FTP)
A TCP/IP protocol specifying the transfer of text or binary files across the network.
## Filter
A filter is used to specify which packets will or will not be used. It can be used in sniffers to determine which packets get displayed, or by firewalls to determine which packets get blocked.
## Filtering Router
An inter-network router that selectively prevents the passage of data packets according to a security policy. A filtering router may be used as a firewall or part of a firewall. A router usually receives a packet from a network and decides where to forward it on a second network. A filtering router does the same, but first decides whether the packet should be forwarded at all, according to some security policy. The policy is implemented by rules (packet filters) loaded into the router.
## Finger
A protocol to lookup user information on a given host. A Unix program that takes an e-mail address as input and returns information about the user who owns that e-mail address. On some systems, finger only reports whether the user is currently logged on. Other systems return additional information, such as the user's full name, address, and telephone number. Of course, the user must first enter this information into the system. Many e-mail programs now have a finger utility built into them.
## Fingerprinting
Sending strange packets to a system in order to gauge how it responds to determine the operating system.
## Firewall
A logical or physical discontinuity in a network to prevent unauthorized access to data or resources.
## Flooding
An attack that attempts to cause a failure in (especially, in the security of) a computer system or other data processing entity by providing more input than the entity can process properly.
## Forest
A forest is a set of Active Directory domains that replicate their databases with each other.
## Fork Bomb
A Fork Bomb works by using the fork() call to create a new process which is a copy of the original. By doing this repeatedly, all available processes on the machine can be taken up.
## Form-Based Authentication
Form-Based Authentication uses forms on a webpage to ask a user to input username and password information.
## Forward Lookup
Forward lookup uses an Internet domain name to find an IP address.
## Forward Proxy
Forward Proxies are designed to be the server through which all requests are made.
## Forwarded Email
Email that was sent to another email address using the email forwarding operation.
## Fragment Offset
The fragment offset field tells the sender where a particular fragment falls in relation to other fragments in the original larger packet.
## Fragment Overlap Attack
A TCP/IP Fragmentation Attack that is possible because IP allows packets to be broken down into fragments for more efficient transport across various media. The TCP packet (and its header) are carried in the IP packet. In this attack the second fragment contains incorrect offset. When packet is reconstructed, the port number will be overwritten.
## Fragmentation
The process of storing a data file in several "chunks" or fragments rather than in a single contiguous sequence of bits in one place on the storage medium.
## Frames
Data that is transmitted between network points as a unit complete with addressing and necessary protocol control information. A frame is usually transmitted serial bit by bit and contains a header field and a trailer field that "frame" the data. (Some control frames contain no data.)
## Full Duplex
A type of duplex communications channel which carries data in both directions at once. Refers to the transmission of data in two directions simultaneously. Communications in which both sender and receiver can send at the same time.
## Fully-Qualified Domain Name
A Fully-Qualified Domain Name is a server name with a hostname followed by the full domain name.
## Fuzzing
The use of special regression testing tools to generate out-of-spec input for an application in order to find security vulnerabilities. Also see "regression testing".
